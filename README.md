# Cryptowallet

### how to install.
You should have *[python 3.9](https://www.python.org/downloads/release/python-390/)* minimum and *[sqlite3](https://doc.ubuntu-fr.org/sqlite)*.
Then:  
Download the wheel package [here](https://github.com/Peskoo/cryptowallet/releases) and  
```bash
$ python3.9 -m pip install cryptowallet-0.1.0-py3-none-any.whl 
```

### basic usage.
- First step, please create a user with:  
`cryptowallet add-user`

- Add some crypto with:  
`cryptowallet add-coin --coin bictoin --value 1 --date 18-03-2021`

- Check your wallet:  
`cryptowallet wallet`

- Check the profit for all your specific coins:  
`cryptowallet profit --coin bictoin`

- Update/Delete with ids:  
Ids are available with `cryptowallet wallet` then:  
`cryptowallet update ...`
`cryptowallet delete ...`

### help.
`cryptowallet --help`
