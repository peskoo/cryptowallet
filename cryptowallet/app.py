import click

import cryptowallet.services.coin as coin_
import cryptowallet.services.user as user_
import cryptowallet.tools.database as db
import cryptowallet.tools.interface as interface
import cryptowallet.tools.operations as op


@click.group()
def cli():
    db.init_database()


@cli.command()
@click.option(
    "--currency", default="eur", type=str, help="Enter your currency [default: eur]."
)
def add_user(currency: str) -> str:
    """/!\\ You must add a user to use this application."""
    user_.create(currency, username="default")
    click.echo("User created.")


@cli.command()
@click.option(
    "-c", "--coin", required=True, type=str, help="Enter the coin name [ex: bitcoin]."
)
@click.option(
    "-d",
    "--date",
    type=str,
    help="Enter when you bought it with this format DAY-MONTH-YEAR [ex: 25-06-1988].",
)
@click.option("-v", "--value", required=True, type=float, help="Enter the value.")
def add_coin(coin: str, date: str, value: float) -> str:
    """Insert new cryptomonnaie into database."""
    coin = coin_.create(entrydate=date, name=coin, value=value)
    click.echo(interface.add_coin(coin))


@cli.command()
def wallet() -> str:
    """Retrieve all the coins owned by the user."""
    coins_list = coin_.get_all()
    user = user_.get("default")
    res = []
    for c in coins_list:
        res.append(
            {
                "id": c.id,
                "coin": c.name,
                "value": c.value,
                "profit": op.get_profit_for_coin(
                    coin_id=c.name,
                    currency=user.currency,
                    date=c.entrydate,
                    value=c.value,
                ),
                "entrydate": c.entrydate,
                "currency": user.currency,
            }
        )
    if res:
        click.echo(interface.wallet(res))
    else:
        click.echo("Your wallet is empty.")


@cli.command()
@click.option("-c", "--coin", type=str, help="Enter the coin name [ex: bitcoin].")
def profit(coin: str) -> str:
    """Difference between the buying date and today for all your coins with specific name."""
    user = user_.get("default")
    coins_list = coin_.get_by_name(coin)
    currency = user.currency
    results = []
    for _coin in coins_list:
        coin_id = _coin.name
        value = _coin.value
        date = _coin.entrydate
        results.append(op.get_profit_for_coin(coin_id, currency, date, value))

    click.echo(interface.profit(results=results, coin=coin_id, currency=currency))


@cli.command()
@click.option(
    "--id",
    type=int,
    required=True,
    help="Enter the coin id you want to delete, show ids with wallet command [ex: 2].",
)
def delete(id: int) -> str:
    """Delete the coin you want with his id."""
    coin_.delete(id)
    click.echo("Coin deleted.")


@cli.command()
@click.option(
    "--id",
    type=int,
    help="Enter the coin id you want to update, show ids with wallet command [ex: 2].",
)
@click.option("-c", "--coin", type=str, help="Enter the new coin name [ex: bitcoin].")
@click.option(
    "-d",
    "--date",
    type=str,
    help="Update when you bought it with this format DAY-MONTH-YEAR [ex: 25-06-1988].",
)
@click.option("-v", "--value", type=float, help="Enter the new value.")
@click.option(
    "--currency",
    type=str,
    help="Enter the new currency for your account [ex: eur].",
)
def update(id: int, coin: str, value: float, date: str, currency: str):
    """Update all informations."""
    if currency:
        user_.update(currency)

    if coin or value or date:
        coin_.update(id=id, name=coin, value=value, date=date)
    click.echo("Coin updated.")


if __name__ == "__main__":
    cli()
