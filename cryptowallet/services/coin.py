from typing import Optional

from pony.orm import select

import cryptowallet.model as model
import cryptowallet.services.coingecko as cg
import cryptowallet.services.user as user_
import cryptowallet.tools.database as db
import cryptowallet.tools.utils as utils


@db.db_session
def create(entrydate: str, name: str, value: int) -> None:
    user = user_.get("default")
    if user is None:
        raise Exception(
            'You should create a user first. Please use the command "add-user"'
        )

    if (
        cg.coin_is_valid(name)
        and utils.date_is_valid(entrydate)
        and utils.value_is_valid(value)
    ):
        return model.Coin(name=name, value=value, entrydate=entrydate, user=user.id)


@db.db_session
def get_all() -> list[model.Coin]:
    """Retrieve all coins owned by the default user."""
    return select(c for c in model.Coin).fetch()


@db.db_session
def get_by_name(coin: str) -> list[model.Coin]:
    """Retrieve all coins with a specific name."""
    return select(c for c in model.Coin if c.name == coin).fetch()


@db.db_session
def get_by_id(id: int) -> model.Coin:
    """Retrieve all coins with a specific name."""
    coin = model.Coin.get(id=id)
    if not coin:
        raise AttributeError("This ID does not exist.")
    return coin


@db.db_session
def update(
    id: int,
    name: Optional[str] = None,
    value: Optional[float] = None,
    date: Optional[str] = None,
):
    if id is None:
        raise AttributeError("You must enter an ID for this operation.")

    coin = get_by_id(id)
    if coin:
        if name and cg.coin_is_valid(name):
            coin.name = name
        if value and utils.value_is_valid(value):
            coin.value = value
        if date and utils.date_is_valid(date):
            coin.entrydate = date


@db.db_session
def delete(id: int):
    """Delete the coin with his id."""
    get_by_id(id).delete()
