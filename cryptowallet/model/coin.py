from pony.orm import Required

import cryptowallet.tools.database

from .user import User  # import direct User to avoid circular import.


class Coin(cryptowallet.tools.database.db.Entity):
    user = Required(User)
    name = Required(str)
    value = Required(float)
    entrydate = Required(str)
